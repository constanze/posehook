@file:Suppress("PrivatePropertyName")

package com.hollyhook.posehook

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.SurfaceTexture
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.util.TypedValue
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.mediapipe.components.CameraHelper.CameraFacing
import com.google.mediapipe.components.CameraXPreviewHelper
import com.google.mediapipe.components.ExternalTextureConverter
import com.google.mediapipe.components.FrameProcessor
import com.google.mediapipe.components.PermissionHelper
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmarkList
import com.google.mediapipe.framework.AndroidAssetUtil
import com.google.mediapipe.framework.Packet
import com.google.mediapipe.framework.PacketGetter
import com.google.mediapipe.framework.ProtoUtil
import com.google.mediapipe.glutil.EglManager
import com.google.protobuf.InvalidProtocolBufferException
import com.hollyhook.posehook.BodyPartSetup.Companion.getBodyPartOfMenuIem
import com.hollyhook.posehook.BodyPartSetup.Companion.oscAddresses
import com.illposed.osc.OSCMessage
import com.illposed.osc.transport.udp.OSCPortOut
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.LegendRenderer
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.io.IOException
import java.net.InetAddress
import java.net.UnknownHostException
import java.text.DateFormat
import java.util.*
import kotlin.concurrent.fixedRateTimer
import kotlin.random.Random.Default.nextInt


class PoseHookFragment :
    Fragment(),
    ActivityCompat.OnRequestPermissionsResultCallback,
    SharedPreferences.OnSharedPreferenceChangeListener {

    // unique names in the osc address
    private lateinit var myOscAddress:String

    // for replaying recording
    private var replayTimer: Timer? = null
    private val replayTimerPeriod = 90L // ms, approx frame rate

    /** Tag for the [Log]. */
    private val TAG = "PoseHookActivity"


    private val BINARY_GRAPH_NAME = "pose_tracking_gpu.binarypb"
    private val INPUT_VIDEO_STREAM_NAME = "input_video"
    private val OUTPUT_VIDEO_STREAM_NAME = "output_video"
    private val OUTPUT_LANDMARKS_STREAM_NAME = "pose_landmarks"

    // Flips the camera-preview frames vertically before sending them into FrameProcessor to be
    // processed in a MediaPipe graph, and flips the processed frames back when they are displayed.
    // This is needed because OpenGL represents images assuming the image origin is at the bottom-left
    // corner, whereas MediaPipe in general assumes the image origin is at top-left.
    private val FLIP_FRAMES_VERTICALLY = true

    init {
        // Load all native libraries needed by mediapipe.
        System.loadLibrary("mediapipe_jni")
        System.loadLibrary("opencv_java3")
    }


    /** needed for scrolling of the graph */
    private val invisibleSeries = PointsGraphSeries<DataPoint>() // invisible

    /** keep the recording sorted along the time stamps */
    private var recordedMeasurements: NavigableMap<Long, Measurement?>? = null

    /** key points for graph (both x and y) */
    private var lifeSeries: KeyPointSeries? = null
    private var playbackSeries: KeyPointSeries? = null

    private var graphView: GraphView? = null
    private var selectedBodyPart = BodyPart.NOSE
    private var fab: FloatingActionButton? = null // replay / stop replay

    /** OSC sending object */
    private var sender: OSCPortOut? = null
    private var combineOsc: Boolean = COMBINE_OSC_DEFAULT
    private var oneFloatOsc: Boolean = ONE_FLOAT_OSC_DEFAULT

    private var mirrorOsc: Boolean = MIRROR_OSC_DEFAULT

    // show error during sending of osc only once
    private var errorShown: Boolean = false

    /** {@link SurfaceTexture} where the camera-preview frames can be accessed. */
    private var previewFrameTexture: SurfaceTexture? = null

    /** Creates and manages an {@link EGLContext}. */
    private var eglManager: EglManager? = null

    /** Sends camera-preview frames into a MediaPipe graph for processing, and
    displays the processed frames onto a {@link Surface}. */
    private var processor: FrameProcessor? = null

    // Converts the GL_TEXTURE_EXTERNAL_OES texture from Android camera into a regular texture to be
    // consumed by {@link FrameProcessor} and the underlying MediaPipe graph.
    private var converter: ExternalTextureConverter? = null

    // Handles camera access via the {@link CameraX} Jetpack support library.
    private var cameraHelper: CameraXPreviewHelper? = null

    /** A [SurfaceView] for camera preview.   */
    private var previewDisplayView: SurfaceView? = null

    /** which camera to take, front or back */
    private var takeFrontCamera = TAKE_FRONT_CAMERA_DEFAULT

    private var orientationEventListener:MyOrientationEventListener? = null

    private var startTimestamp: Long = System.currentTimeMillis()

    // get changes in device orientation to adjust coordinates accordingly
    inner class MyOrientationEventListener(context: Context):
            OrientationEventListener(context) {

        var displayRotation = 0

        /**
         * called on sensor update, approx every 250 ms
         * @returns 0 if portrait 12 o'clock, 270 if device is turned to 3 o'clock etc.
         */
        override fun onOrientationChanged(orientation: Int) {
            // Log.v(this@PoseHookFragment.TAG, "onOrientationChanged $orientation") //called too often

            // Monitors orientation values to determine the target rotation value
            displayRotation = when (orientation) {
                in 45..134 -> Surface.ROTATION_270
                in 135..224 -> Surface.ROTATION_180
                in 225..314 -> Surface.ROTATION_90
                else ->        Surface.ROTATION_0
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_posehook, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i(TAG, "onViewCreated")
        val parent: MainActivity = context as MainActivity

        myOscAddress = createCombinedOscAddress(parent.sharedPref!!)

        // restore what camera to take
        takeFrontCamera = parent.sharedPref!!.getBoolean(
                getString(R.string.take_front_camera_key),
                TAKE_FRONT_CAMERA_DEFAULT
        ) // default is back camera

        setupFab(view)
        setupPreviewDisplayView(view)
        setupMediaPipe()
        setupGraphView(view)

    }

    // return the newly created combined osc address
    private fun createCombinedOscAddress(sharedPref: SharedPreferences): String {
        val str =  sharedPref.getString(
                getString(R.string.combined_address_key),
                COMBINE_OSC_ADDRESS_DEFAULT
        )
        return str!!.replace("%a",
                COMBINE_OSC_ADDRESS_NAMES.random() + "-" + nextInt(10, 99))
    }

    // replay recording button
    private fun setupFab(view: View) {
        recordedMeasurements = TreeMap<Long, Measurement>(compareBy { it })
        fab = view.findViewById(R.id.fab)
        fab?.setOnClickListener { v ->
            if (replayTimer == null) { // replay not yet running
                if (recordedMeasurements == null || recordedMeasurements!!.size <=1 ) {
                    Snackbar.make(v, R.string.no_pose_detected, Snackbar.LENGTH_LONG)
                        .show()
                } else {
                    startReplay()
                }
            } else {
                stopReplay()
            }
        }
    }

    // called on click to fab button while replay is running
    private fun stopReplay() {
        Log.v(TAG, "end replay")

        // stop replay, go live again
        replayTimer?.cancel()
        replayTimer = null
        if (context != null) {
            // show the play button
            val d = ContextCompat.getDrawable(context!!, android.R.drawable.ic_media_play)
            fab?.setImageDrawable(d)
        }

        if (playbackSeries != null) {
            graphView?.removeSeries(playbackSeries!!.dataSeriesX)
            graphView?.removeSeries(playbackSeries!!.dataSeriesY)
            playbackSeries = null
        }
    }

    /** called when fab is pressed, and there are some messages recorded */
    private fun startReplay() {
        // change color of the plot
        playbackSeries = KeyPointSeries() // holds x and y
            .color(ContextCompat.getColor(context!!, R.color.colorAccent))
            .bodyPart(selectedBodyPart)
            .strokeWidth(1.1f)
            .setInLegend(false)
            .size(dip2pixel(PLOT_SHAPE_SIZE.toFloat()))

        graphView?.addSeries(playbackSeries?.dataSeriesX)
        graphView?.addSeries(playbackSeries?.dataSeriesY)

        val replayStartingTime = System.currentTimeMillis()
        val recordingDuration = recordedMeasurements!!.lastKey() -
                recordedMeasurements!!.firstKey()

        Log.v(
                TAG, "looping over " + recordedMeasurements!!.size +
                " OSC messages of $recordingDuration ms for " + playbackSeries?.bodyPart
        )

        if (context != null) {
            // we are playing, show the pause button
            val d = ContextCompat.getDrawable(context!!, android.R.drawable.ic_media_pause)
            fab?.setImageDrawable(d)
        }

        // every 100ish ms re-send the recorded messages, do it with the same
        // timing than they were recorded
        replayTimer = fixedRateTimer(
                "timer",
                false,
                0, replayTimerPeriod
        ) {
            val now = System.currentTimeMillis()

            // time offset where we are during replay right now
            val offset = if (recordingDuration == 0L) 0
                else (now - replayStartingTime) % recordingDuration

            // detect relevant time interval in recording. Can be more than 1..

            // floorKey: find greatest key less than or equal to the given key, or
            // null if there is no such key.
            val start =
                recordedMeasurements!!.floorKey(recordedMeasurements!!.firstKey() + offset)
            val end =
                recordedMeasurements!!.floorKey(recordedMeasurements!!.firstKey() + offset + replayTimerPeriod)

            if (start != null && end != null)
                recordedMeasurements!!.subMap(start, false, end, true)
                                  .forEach { (_, m) ->
                    try {
                        //Log.d(TAG, "resend message @ $t ms, realtime is $offset")
                        sendMeasurement(m!!)
                        plotData(m, playbackSeries)
                    } catch (ex: Exception) {
                        Log.e(TAG, "Error resending OSC message: " + ex.message)
                    }
                }
        }
    }


    private fun setupMediaPipe() {
        // Initialize asset manager so that MediaPipe native libraries can access the app assets, e.g.,
        // binary graphs.
        AndroidAssetUtil.initializeNativeAssetManager(context)

        // see https://github.com/google/mediapipe/issues/1013caseissuecomment-719344764
        ProtoUtil.registerTypeName(
                NormalizedLandmarkList::class.java,
                "mediapipe.NormalizedLandmarkList"
        )

        eglManager = EglManager(null)
        processor = FrameProcessor(
                context,
                eglManager!!.nativeContext,
                BINARY_GRAPH_NAME,
                INPUT_VIDEO_STREAM_NAME,
                OUTPUT_VIDEO_STREAM_NAME
        )
        processor!!.videoSurfaceOutput.setFlipY(FLIP_FRAMES_VERTICALLY)

        // configuration input - not working
        /*val packetCreator = processor!!.packetCreator
        val inputSidePackets: MutableMap<String, Packet> = HashMap()
        inputSidePackets[INPUT_UPPER_SIDE_PACKET_NAME] = packetCreator.createBool(true)
        processor!!.setInputSidePackets(inputSidePackets) */

        // landmark output
        processor!!.addPacketCallback(
                OUTPUT_LANDMARKS_STREAM_NAME
        ) { packet: Packet ->
            if (replayTimer == null ) { // replay is not active
                try {
                    val m = Measurement(mirrorOsc)
                    val poseLandmarks =
                        PacketGetter.getProto(
                                packet,
                                NormalizedLandmarkList::class.java
                        )

                    // network activity on media pipe thread?

                    // transfer data into my list
                    for (landmark in poseLandmarks.landmarkList) {
                        m.addLandmark(landmark, orientationEventListener?.displayRotation)
                    }

                    // save in recording
                    val now = System.currentTimeMillis()
                    val f: Float = (now - startTimestamp).toFloat()
                    m.addTimestamp(f)
                    recordMeasurement(now, m)

                    // send out OSC and plot
                    sendMeasurement(m)
                    plotData(m)
                } catch (exception: InvalidProtocolBufferException) {
                    Log.e(TAG, "Failed to get proto.", exception)
                }
            }
        }
    }


    private fun setupGraphView(view: View): MainActivity {
        // popup menu to select what to plot
        graphView = view.findViewById(R.id.chartView) as GraphView
        activity?.registerForContextMenu(graphView)

        // this is only for nice horizontal scrolling -  not used currently
        invisibleSeries.color = Color.TRANSPARENT
        invisibleSeries.isInLegend = false
        invisibleSeries.appendData(DataPoint(Date(System.currentTimeMillis()), 0.0), true, DATA_LEN)

        // setup graph view
        graphView?.addSeries(invisibleSeries) // This must be done first, to prevent 1.1.1970 label on the x axis

        // as we use dates as labels, the human rounding to nice readable numbers
        // is not necessary, crashes anyway
        graphView?.gridLabelRenderer?.setHumanRounding(false)

        graphView?.viewport?.isYAxisBoundsManual = true
        graphView?.viewport?.setMinY(0.0)
        graphView?.viewport?.setMaxY(1.0)

        // activate horizontal scrolling, unit is ms
        graphView?.viewport?.isXAxisBoundsManual =
            true // this allows automated scrolling to the end
        val now = System.currentTimeMillis().toDouble()
        graphView?.viewport?.setMinX(now)
        graphView?.viewport?.setMaxX(now + 33000.0) // milliseconds, one bar is always visible

        graphView?.legendRenderer?.isVisible = true
        graphView?.legendRenderer?.align = LegendRenderer.LegendAlign.TOP

        // add transparency
        val col = ContextCompat.getColor(context!!, R.color.colorPrimary) and(0x1FFFFFFF)
        graphView?.legendRenderer?.backgroundColor = col
        graphView?.legendRenderer?.textSize = dip2pixel(18f)

        // set time axis label formatter
        val df = DateFormat.getTimeInstance(DateFormat.SHORT)
        graphView?.gridLabelRenderer?.labelFormatter = DateAsXAxisLabelFormatter(activity, df)
        graphView?.gridLabelRenderer?.numHorizontalLabels =
            3 // only 2 because who cares, the 3rd is not working

        // find out what to draw in graph view, the parent owns the preferences
        val parent: MainActivity = context as MainActivity
        // gets called if something else to draw gets selected
        parent.sharedPref?.registerOnSharedPreferenceChangeListener(this)

        val selected = parent.sharedPref!!.getInt(
                getString(R.string.context_menu_key), PLOT_BODY_PART_DEFAULT
        )
        combineOsc = parent.sharedPref!!.getBoolean(
            getString(R.string.osc_combined_key), COMBINE_OSC_DEFAULT)
        mirrorOsc = parent.sharedPref!!.getBoolean(
            getString(R.string.mirror_key), MIRROR_OSC_DEFAULT)
        oneFloatOsc = parent.sharedPref!!.getBoolean(
            getString(R.string.one_float_osc_key), ONE_FLOAT_OSC_DEFAULT)


        setupGraphSeries(getBodyPartOfMenuIem(selected))
        return parent
    }

    private fun setupPreviewDisplayView(view: View) {
        previewDisplayView = view.findViewById(R.id.surfaceView)
        previewDisplayView!!.visibility = View.GONE
        Log.d(TAG, "previewDisplayView is set to 'gone'. Attaching SurfaceHolder.Callback")
        val h = previewDisplayView!!.holder

        h.setFixedSize(720, 720)
        h.addCallback(
                object : SurfaceHolder.Callback {
                    override fun surfaceCreated(holder: SurfaceHolder) {
                        Log.v(TAG, "SurfaceHolder.Callback: surfaceCreated")
                        processor!!.videoSurfaceOutput.setSurface(holder.surface)
                    }

                    override fun surfaceChanged(holder: SurfaceHolder,
                                                format: Int,
                                                width: Int,
                                                height: Int
                    ) {
                        Log.v(TAG, "SurfaceHolder.Callback: surfaceChanged w/h $width/$height")
                        onPreviewDisplaySurfaceChanged(holder, format, width, height)
                    }

                    override fun surfaceDestroyed(holder: SurfaceHolder) {
                        Log.v(TAG, "SurfaceHolder.Callback: surfaceDestroyed")
                        processor!!.videoSurfaceOutput.setSurface(null)
                    }
                }
        )
    }

    private fun computeViewSize(width: Int, height: Int): Size {
        return Size(width, height)
    }

    // used in surfaceChanged callback
    @Suppress("UNUSED_PARAMETER")
    fun onPreviewDisplaySurfaceChanged(
            holder: SurfaceHolder?, format: Int, width: Int, height: Int
    ) {
        // (Re-)Compute the ideal size of the camera-preview display (the area that the
        // camera-preview frames get rendered onto, potentially with scaling and rotation)
        // based on the size of the SurfaceView that contains the display.
        val viewSize = computeViewSize(width, height)
        val displaySize = cameraHelper!!.computeDisplaySizeFromViewSize(viewSize)
        val isCameraRotated = cameraHelper!!.isCameraRotated
        Log.v(TAG, "onPreviewDisplaySurfaceChanged $displaySize isRotated $isCameraRotated")

        // Connect the converter to the camera-preview frames as its input (via
        // previewFrameTexture), and configure the output width and height as the computed
        // display size.
        converter!!.setSurfaceTextureAndAttachToGLContext(
                previewFrameTexture,
                if (isCameraRotated) displaySize.height else displaySize.width,
                if (isCameraRotated) displaySize.width else displaySize.height
        )
    }

    private fun cameraTargetResolution(): Size {
        return Size(720, 720)
        // return null = No preference and let the camera (helper) decide.
    }

    // cameraHelper listener function, called from media pipe
    private fun onCameraStarted(surfaceTexture: SurfaceTexture) {
        Log.v(TAG, "onCameraStarted got surfaceTexture $surfaceTexture")
/*
        // see https://github.com/google/mediapipe/issues/818
        val viewGroup = previewDisplayView!!.parent as ViewGroup
        viewGroup.removeView(previewDisplayView)
        viewGroup.addView(previewDisplayView)
*/
        previewFrameTexture = surfaceTexture
        // Make the display view visible to start showing the preview. This triggers the
        // SurfaceHolder.Callback added to (the holder of) previewDisplayView.
        previewDisplayView!!.visibility = View.VISIBLE
        Log.d(TAG, "previewDisplayView is visible")
    }

    private fun startCamera() {
        cameraHelper = CameraXPreviewHelper()
        cameraHelper!!.setOnCameraStartedListener { surfaceTexture: SurfaceTexture? ->
            onCameraStarted(surfaceTexture!!)
        }
        val cameraFacing = if (takeFrontCamera) CameraFacing.FRONT else CameraFacing.BACK
        cameraHelper!!.startCamera(
                activity,
                cameraFacing,
                null,
                cameraTargetResolution()
        )
    }


    // configure GraphView properties that depend on the series selected
    private fun setupGraphSeries(selected: BodyPart) {
        Log.v(TAG, "setupGraphSeries for $selected")

        // remove current series, if any
        graphView?.removeSeries(lifeSeries?.dataSeriesX)
        graphView?.removeSeries(lifeSeries?.dataSeriesY)

        lifeSeries = KeyPointSeries() // holds x and y
            .color(ContextCompat.getColor(context!!, R.color.colorPrimary))
            .bodyPart(selected)
            .bodyPartName(getString(BodyPartSetup.getBodyPartNameId(selected)))
            .strokeWidth(1.5f)
            .size(dip2pixel(PLOT_SHAPE_SIZE.toFloat()))

        graphView?.addSeries(lifeSeries?.dataSeriesX)
        graphView?.addSeries(lifeSeries?.dataSeriesY)
        graphView?.legendRenderer?.textColor = Color.DKGRAY

        // this function has the side effect that the legend width is freshly calculated
        graphView?.legendRenderer?.textSize = graphView?.legendRenderer?.textSize!!

        selectedBodyPart = selected

        if (replayTimer != null) {
            stopReplay()
            startReplay()
        }

    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume")
        startOsc()

        converter = ExternalTextureConverter(eglManager?.context)
        converter!!.setFlipY(FLIP_FRAMES_VERTICALLY)
        converter!!.setConsumer(processor)
        if (PermissionHelper.cameraPermissionsGranted(activity)) {
            startCamera()
        } else {
            Log.w(TAG, "camera permission not granted")
        }
    }

    override fun onPause() {
        Log.i(TAG, "onPause")
        super.onPause()
        stopReplay()
        try{
            converter?.close()
            previewFrameTexture?.release()
        }
        catch (e: Exception) {
            e.printStackTrace()
        }

        stopOSC()

        // Hide preview display until we re-open the camera again.
        previewDisplayView?.visibility = View.GONE
    }

    // life cycle
    override fun onDestroy() {
        Log.i(TAG, "onDestroy")
        super.onDestroy()
    }


    /**
     * called if preferences are changed in any of the dialogs.
     * Re-read saved values and react
     */
    override fun onSharedPreferenceChanged(pref: SharedPreferences?, key: String?) {
        try {
            when (key) {
                getString(R.string.context_menu_key) -> {
                    val selected = pref!!.getInt(key, R.id.nose)
                    setupGraphSeries(getBodyPartOfMenuIem(selected))
                }
                getString(R.string.mirror_key) -> {
                    mirrorOsc = pref!!.getBoolean(key, MIRROR_OSC_DEFAULT)
                }
                getString(R.string.osc_combined_key) -> {
                    combineOsc = pref!!.getBoolean(key, COMBINE_OSC_DEFAULT)
                }
                getString(R.string.one_float_osc_key) -> {
                    oneFloatOsc = pref!!.getBoolean(key, ONE_FLOAT_OSC_DEFAULT)
                }
                getString(R.string.combined_address_key) -> {
                    myOscAddress = createCombinedOscAddress(pref!!)
                }
                getString(R.string.port_key),
                getString(R.string.ip_key) -> {
                    if (sender != null) {
                        stopOSC()
                        startOsc()
                    }
                }
                getString(R.string.take_front_camera_key) -> {

                    // toggle camera, FIXME check if we have 2 cameras
                    Log.i(TAG, "close " + (if (takeFrontCamera) "front" else "back") + " camera")

                    previewDisplayView?.visibility = View.GONE


                    try {
                        converter?.close()
                        previewFrameTexture?.release()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    takeFrontCamera = !takeFrontCamera

                    // open again the other cam ---------------------------------
                    Log.i(TAG, "open " + (if (takeFrontCamera) "front" else "back") + " camera")

                    converter = ExternalTextureConverter(eglManager?.context)
                    converter!!.setFlipY(FLIP_FRAMES_VERTICALLY)
                    converter!!.setConsumer(processor)
                    cameraHelper = CameraXPreviewHelper()
                    cameraHelper!!.setOnCameraStartedListener { surfaceTexture: SurfaceTexture? ->
                        onCameraStarted(surfaceTexture!!)
                    }
                    val cameraFacing = if (takeFrontCamera) CameraFacing.FRONT else CameraFacing.BACK
                    cameraHelper!!.startCamera(
                            activity,
                            cameraFacing,
                            null,
                            cameraTargetResolution()
                    )
                }
                else -> {
                    // all others are OSC addresses, setup the whole thing again
                    BodyPartSetup.initConfig(requireContext(), pref!!)
                }
            }
        } catch (ise: java.lang.IllegalStateException) {
            // just ignore, happens from time to time that the context is not valid
            // don't know why, don't know how to fix.
        }
    }

    private fun dip2pixel(dipValue: Float): Float {
        if (context == null) return 0f // already killed
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dipValue,
                resources.displayMetrics
        )
    }

    /**
     * Shows a [Toast] on the UI thread.
     * @param text The message to show
     */
    private fun showToast(text: String) {
        val activity = activity
        activity?.runOnUiThread { Toast.makeText(activity, text, Toast.LENGTH_SHORT).show() }
    }

    // to get the display rotation
    override fun onStart() {
        super.onStart()
        Log.i(TAG, "onStart")
        orientationEventListener = MyOrientationEventListener(
                requireContext()
        )
        orientationEventListener?.enable()
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "onStop")
        orientationEventListener?.disable()
        orientationEventListener = null
    }


    //--- OSC -----------------------------------------------------------

    /**
     * aim to quickly return the address w/o much checking
     * will be empty if osc is off
     */
    private fun getAddressOfBP(bp: Int): String {
        return oscAddresses[bp]
    }

    // used for recording and live data
    private fun sendMeasurement(landmarks: Measurement) {
        if (sender == null) return
        if (combineOsc) {
            val msg = OSCMessage(myOscAddress, landmarks.argCombined)
            sendOsc(msg)
        } else {
            // send individual messages per body part
            val bodyParts = BodyPart.values().dropLast(1)
            for (bp in bodyParts) {
                val address = getAddressOfBP(bp.ordinal)
                if (address.isBlank()) continue
                val landmark = landmarks.getLandmark(bp)

                if (oneFloatOsc) {
                    val i = listOf("x", "y", "z", "visibility").iterator()
                    for (lm in landmark) {
                        if (i.hasNext()) {
                            val x = List(1) { lm }
                            sendOsc(OSCMessage("$address/" + i.next(), x))
                        }
                    }
                } else {
                    sendOsc(OSCMessage(address, landmark))
                }
            } // end for each landmark
        }
    }

    /**
     * save landmark measurement in recording, which is a map indexed by time stamp
     * */
    private fun recordMeasurement(
            now: Long,
            msg: Measurement
    ) {
        if (recordedMeasurements!!.size > NUM_OF_RECORDED_MESSAGES) {
            // we have max number of data, calculate frame rate
            if (now.div(1000) != recordedMeasurements!!.lastKey().div(1000)) {
                // once per second calculate frame rate
                val duration = recordedMeasurements!!.lastKey() - recordedMeasurements!!.firstKey()
                val fps = 1000.div(duration.div(NUM_OF_RECORDED_MESSAGES))
                activity?.runOnUiThread {
                    // this needs to be removed some time after receiving the last frame
                    activity?.title = "$fps fps"
                    Log.v(TAG, "$fps fps")
                }
            }

            // removes first which is oldest
            recordedMeasurements!!.pollFirstEntry()
        }

        // append map entry with key as current timestamp
        recordedMeasurements!![now] = msg
    }


    private fun sendOsc(msg: OSCMessage) {
        try {
            if (sender != null)
                sender!!.send(msg)
        } catch (ex: Exception) {
            Log.e(TAG, "Error sending OSC message: " + ex.message)
            if (!errorShown)
                showToast((getString(R.string.error_socket, ex.message ?: ex.toString())))
            errorShown = true // otherwise the toasts are shown for ever
            // stop OSC? or live with the hickup until things get better?
        }
    }


    /**
     * create a OSCSender object opening the network connection, or display
     * an error if it doesn't work. Uses a separate thread for communication.
     */
    private fun startOsc() {
        errorShown = false
        val parent: MainActivity = context as MainActivity
        val ip = parent.sharedPref!!.getString(
                getString(R.string.ip_key), getString(R.string.ip_default)
        )
        // likewise for port
        val port = parent.sharedPref!!.getInt(
                getString(R.string.port_key), getString(R.string.port_default).toInt()
        )

        if (ip == null || ip == "") {
            // no ip given, don't start
            AlertDialog.Builder(context)
                .setMessage(R.string.error_ip)
                .show()
            return
        }
        val thread = Thread {
            try {
                val host: InetAddress
                try {
                    host = InetAddress.getByName(ip)
                    sender = OSCPortOut(host, port)
                    Log.i(TAG, "OSC Port opened")
                } catch (e1: UnknownHostException) {
                    showToast(getString(R.string.error_unknown_host, ip))
                    sender = null
                } catch (e2: IOException) {
                    showToast(getString(R.string.error_io_exception, ip, port))
                    sender = null
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

        thread.start()
    }

    private fun stopOSC() {
        if (sender != null) {
            sender!!.close()
            sender = null
        }
    }

    /**
     * plot data ot a selected body part, taken out of the recorded osc messages
     * @param meas as previously recorded
     * @param series optional data [KeyPointSeries], defaults to life series in contrast to the recorded series
     */
    private fun plotData(meas: Measurement, series: KeyPointSeries? = lifeSeries) {
        val x = meas.getX(selectedBodyPart)
        val y = meas.getY(selectedBodyPart)
        plotData(x, y, series)
    }

    private fun plotData(x: Float, y: Float, series: KeyPointSeries?) {
        val stepCounter = System.currentTimeMillis()
        val now = Date(stepCounter)

        activity?.runOnUiThread {
            //invisibleSeries.appendData(DataPoint(now, 0.0), true, DATA_LEN)
            series?.appendData(now, x, y)
        }
    }
}

