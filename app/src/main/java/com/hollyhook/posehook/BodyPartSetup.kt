package com.hollyhook.posehook

import android.content.Context
import android.content.SharedPreferences
import android.util.Log



// numbers same as used by media pipe
enum class BodyPart {
    NOSE, // 0
    LEFT_EYE_INNER, LEFT_EYE, LEFT_EYE_OUTER, // 1, 2, 3
    RIGHT_EYE_INNER, RIGHT_EYE, RIGHT_EYE_OUTER, // 4, 5, 6
    LEFT_EAR, RIGHT_EAR,     // 7, 8
    MOUTH_LEFT, MOUTH_RIGHT, // 9, 10
    LEFT_SHOULDER, RIGHT_SHOULDER, // 11, 12
    LEFT_ELBOW, RIGHT_ELBOW, // 13, 14
    LEFT_WRIST, RIGHT_WRIST, // 15, 16
    LEFT_PINKY, RIGHT_PINKY, // 17, 18
    LEFT_INDEX, RIGHT_INDEX, // 19, 20
    LEFT_THUMB, RIGHT_THUMB, // 21, 22
    LEFT_HIP, RIGHT_HIP,     // 23, 24
    LEFT_KNEE, RIGHT_KNEE,   // 25, 26
    LEFT_ANKLE, RIGHT_ANKLE, // 27, 28
    LEFT_HEEL, RIGHT_HEEL,   // 29, 30
    LEFT_FOOT_INDEX, RIGHT_FOOT_INDEX, // 31, 32
    NUM_OF_BODY_PARTS;
}

/**
 * configuration like osc address,.. for a single body part measurement
 */

data class BodyPartConfig(
   val bodyPart: BodyPart, // media pipe id
   val addressDefault: Int,
   val addressKey: Int,
   val editId: Int,
   val contextMenuId: Int,
   val titleId: Int,
   var oscAddress: String? = null) {

    fun restoreOscAddress(context: Context, pref: SharedPreferences) {
        oscAddress = pref.getString(context.getString(addressKey),
            context.getString(addressDefault))
    }
}


sealed class BodyPartSetup(val context: Context,
                           val bodyPartConfigs: List<BodyPartConfig>,
                           var sendOSC: Boolean, // sending data via osc
                           val sendOSCKey: Int,  // in strings.xml, that way avoiding typos
                           var checkBoxID: Int   // id of the check box as defined in dialog layout xml
) {
    class Nose(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.NOSE,
                R.string.nose_default,
                R.string.nose_key,
                R.id.nose,
                R.id.menu_nose,
                R.string.nose_short
            )
        ),
        NOSE_ON_DEFAULT,
        R.string.nose_on_key,
        R.id.nose_on
    )
    
    class Eyes(c: Context): BodyPartSetup(c, 
        listOf(
            BodyPartConfig(BodyPart.LEFT_EYE_INNER,
                R.string.eye_left_inner_default,
                R.string.eye_left_inner_key,
                R.id.eyes_left_inner,
                R.id.menu_eye_left_inner,
                R.string.eye_left_inner
            ),
            BodyPartConfig(BodyPart.LEFT_EYE,
                R.string.eye_left_default,
                R.string.eye_left_key,
                R.id.eyes_left,
                R.id.menu_eye_left,
                R.string.eye_left
            ),
            BodyPartConfig(BodyPart.LEFT_EYE_OUTER,
                R.string.eye_left_outer_default,
                R.string.eye_left_outer_key,
                R.id.eyes_left_outer,
                R.id.menu_eye_left_outer,
                R.string.eye_left_outer
            ),
            BodyPartConfig(BodyPart.RIGHT_EYE_INNER,
                R.string.eye_right_inner_default,
                R.string.eye_right_inner_key,
                R.id.eyes_right_inner,
                R.id.menu_eye_right_inner,
                R.string.eye_right_inner
            ),
            BodyPartConfig(BodyPart.RIGHT_EYE,
                R.string.eye_right_default,
                R.string.eye_right_key,
                R.id.eyes_right,
                R.id.menu_eye_right,
                R.string.eye_right
            ),
            BodyPartConfig(BodyPart.RIGHT_EYE_OUTER,
                R.string.eye_right_outer_default,
                R.string.eye_right_outer_key,
                R.id.eyes_right_outer,
                R.id.menu_eye_right_outer,
                R.string.eye_right_outer
            )
        ),
        EYES_ON_DEFAULT,
        R.string.eyes_on_key,
        R.id.eyes_on
    )

    class Ears(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.LEFT_EAR,
                R.string.ear_left_default,
                R.string.ear_left_key,
                R.id.ears_left,
                R.id.menu_ear_left,
                R.string.ear_left),
            BodyPartConfig(BodyPart.RIGHT_EAR,
                R.string.ear_right_default,
                R.string.ear_right_key,
                R.id.ears_right,
                R.id.menu_ear_right,
                R.string.ear_right)
        ),
        EARS_ON_DEFAULT,
        R.string.ears_on_key,
        R.id.ears_on
    )

    class Mouth(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.MOUTH_LEFT,
                R.string.mouth_left_default,
                R.string.mouth_left_key,
                R.id.mouth_left,
                R.id.menu_mouth_left,
                R.string.mouth_left),
            BodyPartConfig(BodyPart.MOUTH_RIGHT,
                R.string.mouth_right_default,
                R.string.mouth_right_key,
                R.id.mouth_right,
                R.id.menu_mouth_right,
                R.string.mouth_right)
        ),
        MOUTH_ON_DEFAULT,
        R.string.mouth_on_key,
        R.id.mouth_on
    )

    class Shoulders(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.LEFT_SHOULDER,
                R.string.shoulder_left_default,
                R.string.shoulder_left_key,
                R.id.shoulders_left,
                R.id.menu_shoulder_left,
                R.string.shoulder_left),
            BodyPartConfig(BodyPart.RIGHT_SHOULDER,
                R.string.shoulder_right_default,
                R.string.shoulder_right_key,
                R.id.shoulders_right,
                R.id.menu_shoulder_right,
                R.string.shoulder_right)
        ),
        SHOULDER_ON_DEFAULT,
        R.string.shoulders_on_key,
        R.id.shoulders_on
    )

    class Elbows(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.LEFT_ELBOW,
                R.string.elbow_left_default,
                R.string.elbow_left_key,
                R.id.elbows_left,
                R.id.menu_elbow_left,
                R.string.elbow_left),
            BodyPartConfig(BodyPart.RIGHT_ELBOW,
                R.string.elbow_right_default,
                R.string.elbow_right_key,
                R.id.elbows_right,
                R.id.menu_elbow_right,
                R.string.elbow_right)
        ),
        ELBOWS_ON_DEFAULT,
        R.string.elbows_on_key,
        R.id.elbows_on
    )

    class LeftHand(c:Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.LEFT_WRIST,
                R.string.left_hand_wrist_default,
                R.string.left_hand_wrist_key,
                R.id.left_hand_wrist,
                R.id.menu_left_wrist,
                R.string.left_hand_wrist),
            BodyPartConfig(BodyPart.LEFT_PINKY,
                R.string.left_hand_pinky_default,
                R.string.left_hand_pinky_key,
                R.id.left_hand_pinky,
                R.id.menu_left_pinky,
                R.string.left_hand_pinky),
            BodyPartConfig(BodyPart.LEFT_INDEX,
                R.string.left_hand_index_default,
                R.string.left_hand_index_key,
                R.id.left_hand_index,
                R.id.menu_left_index,
                R.string.left_hand_index),
            BodyPartConfig(BodyPart.LEFT_THUMB,
                R.string.left_hand_thumb_default,
                R.string.left_hand_thumb_key,
                R.id.left_hand_thumb,
                R.id.menu_left_thumb,
                R.string.left_hand_thumb)
        ),
        LEFT_HAND_ON_DEFAULT,
        R.string.left_hand_on_key,
        R.id.left_hand_on
    )

    class RightHand(c:Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.RIGHT_WRIST,
                R.string.right_hand_wrist_default,
                R.string.right_hand_wrist_key,
                R.id.right_hand_wrist,
                R.id.menu_right_wrist,
                R.string.right_hand_wrist),
            BodyPartConfig(BodyPart.RIGHT_PINKY,
                R.string.right_hand_pinky_default,
                R.string.right_hand_pinky_key,
                R.id.right_hand_pinky,
                R.id.menu_right_pinky,
                R.string.right_hand_pinky),
            BodyPartConfig(BodyPart.RIGHT_INDEX,
                R.string.right_hand_index_default,
                R.string.right_hand_index_key,
                R.id.right_hand_index,
                R.id.menu_right_index,
                R.string.right_hand_index),
            BodyPartConfig(BodyPart.RIGHT_THUMB,
                R.string.right_hand_thumb_default,
                R.string.right_hand_thumb_key,
                R.id.right_hand_thumb,
                R.id.menu_right_thumb,
                R.string.right_hand_thumb)
        ),
        RIGHT_HAND_ON_DEFAULT,
        R.string.right_hand_on_key,
        R.id.right_hand_on
    )

    class Hips(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.LEFT_HIP,
                R.string.hip_left_default,
                R.string.hip_left_key,
                R.id.hips_left,
                R.id.menu_hip_left,
                R.string.hip_left),
            BodyPartConfig(BodyPart.RIGHT_HIP,
                R.string.hip_right_default,
                R.string.hip_right_key,
                R.id.hips_right,
                R.id.menu_hip_right,
                R.string.hip_right)
        ),
        HIPS_ON_DEFAULT,
        R.string.hips_on_key,
        R.id.hips_on
    )
    
    class Knees(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.LEFT_KNEE,
                R.string.knee_left_default,
                R.string.knee_left_key,
                R.id.knees_left,
                R.id.menu_knee_left,
                R.string.knee_left),
            BodyPartConfig(BodyPart.RIGHT_KNEE,
                R.string.knee_right_default,
                R.string.knee_right_key,
                R.id.knees_right,
                R.id.menu_knee_right,
                R.string.knee_right)
            ),
        KNEES_ON_DEFAULT,
        R.string.knees_on_key,
        R.id.knees_on
    )
    
    class LeftFoot(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.LEFT_ANKLE,
                R.string.left_foot_ankle_default,
                R.string.left_foot_ankle_key,
                R.id.left_foot_ankle,
                R.id.menu_left_foot_ankle,
                R.string.left_foot_ankle),
            BodyPartConfig(BodyPart.LEFT_HEEL,
                R.string.left_foot_heel_default,
                R.string.left_foot_heel_key,
                R.id.left_foot_heel,
                R.id.menu_left_foot_heel,
                R.string.left_foot_heel),
            BodyPartConfig(BodyPart.LEFT_FOOT_INDEX,
                R.string.left_foot_index_default,
                R.string.left_foot_index_key,
                R.id.left_foot_index,
                R.id.menu_left_foot_index,
                R.string.left_foot_index)
            ),
        LEFT_FOOT_ON_DEFAULT,
        R.string.left_foot_on_key,
        R.id.left_foot_on
    )

    class RightFoot(c: Context): BodyPartSetup(c,
        listOf(
            BodyPartConfig(BodyPart.RIGHT_ANKLE,
                R.string.right_foot_ankle_default,
                R.string.right_foot_ankle_key,
                R.id.right_foot_ankle,
                R.id.menu_right_foot_ankle,
                R.string.right_foot_ankle),
            BodyPartConfig(BodyPart.RIGHT_HEEL,
                R.string.right_foot_heel_default,
                R.string.right_foot_heel_key,
                R.id.right_foot_heel,
                R.id.menu_right_foot_heel,
                R.string.right_foot_heel),
            BodyPartConfig(BodyPart.RIGHT_FOOT_INDEX,
                R.string.right_foot_index_default,
                R.string.right_foot_index_key,
                R.id.right_foot_index,
                R.id.menu_right_foot_index,
                R.string.right_foot_index)
        ),
        RIGHT_FOOT_ON_DEFAULT,
        R.string.right_foot_on_key,
        R.id.right_foot_on
    )

    fun getOscAddressOfBodyPart(bp: BodyPart): String? {
        for (bpc in bodyPartConfigs) {
            if (bpc.bodyPart == bp)
                return bpc.oscAddress
        }
        return null
    }

    // (re-)load shared preferences
    fun restorePreferences(prefs: SharedPreferences) {
        for (bp in bodyPartConfigs)
            bp.restoreOscAddress(context, prefs)
        sendOSC = prefs.getBoolean(context.getString(sendOSCKey), sendOSC)
    }


    // static
    companion object {
        private const val TAG = "BodyPartConfigs.companion"

        // list which groups the key points in manageable portions

        // the address as a plain list, index should match landmark id
        val oscAddresses = ArrayList<String>()
        val bodyPartSetups = ArrayList<BodyPartSetup>()

        /**
         * setup the configuration of the individual body parts
         */
        fun initConfig(c: Context, pref: SharedPreferences) {
            bodyPartSetups.clear()
            bodyPartSetups.add(Nose(c))
            bodyPartSetups.add(Eyes(c))
            bodyPartSetups.add(Ears(c))
            bodyPartSetups.add(Mouth(c))
            bodyPartSetups.add(Shoulders(c))
            bodyPartSetups.add(Elbows(c))
            bodyPartSetups.add(LeftHand(c))
            bodyPartSetups.add(RightHand(c))
            bodyPartSetups.add(Hips(c))
            bodyPartSetups.add(Knees(c))
            bodyPartSetups.add(LeftFoot(c))
            bodyPartSetups.add(RightFoot(c))

            for (bps in bodyPartSetups) {
                // read prefs
                bps.restorePreferences(pref)
            }

            updateOscAddresses()
        }

        // build a list of address in the same order than the body parts
        // assumes that osc addresses are already loaded
        private fun updateOscAddresses() {
            oscAddresses.clear()
            for (cnt in BodyPart.values()) {
                var found = false

                // look if it is in any of the setups
                for (bps in bodyPartSetups) {
                    val a = bps.getOscAddressOfBodyPart(cnt)
                    found = a != null
                    if (found) {
                        oscAddresses.add( if (bps.sendOSC) a!! else "" )
                        break
                    }
                }
                if (!found)
                    Log.w(TAG, "something is not right with the BodyPartSetup")
            }
            if (oscAddresses.size != BodyPart.NUM_OF_BODY_PARTS.ordinal)
                Log.w(TAG, "osc address mismatch in BodyPartSetup")

        }

        fun getBodyPartOfMenuIem(itemId: Int): BodyPart {
            // look if it is in any of the setups
            for (bps in bodyPartSetups) {
                for (bp in bps.bodyPartConfigs) {
                    if (bp.contextMenuId == itemId)
                        return bp.bodyPart
                }
            }
            return BodyPart.NOSE
        }

        fun getBodyPartNameId(bp: BodyPart): Int {
            // look if it is in any of the setups
            for (bps in bodyPartSetups) {
                for (bpc in bps.bodyPartConfigs) {
                    if (bpc.bodyPart == bp)
                        return bpc.titleId
                }
            }
            return R.string.nose_short
        }

    }
}




