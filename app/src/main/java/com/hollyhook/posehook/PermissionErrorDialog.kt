package com.hollyhook.posehook

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

/**
 * Shows an error message dialog.
 */
class PermissionErrorDialog : DialogFragment() {

  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
    AlertDialog.Builder(activity)
      .setMessage(arguments!!.getString(ARG_MESSAGE))
      .setPositiveButton(android.R.string.ok) { _, _ -> activity!!.finish() }
      .create()

  companion object {

    @JvmStatic
    private val ARG_MESSAGE = "message"

    @JvmStatic
    fun newInstance(message: String): PermissionErrorDialog = PermissionErrorDialog().apply {
      arguments = Bundle().apply { putString(ARG_MESSAGE, message) }
    }
  }
}