package com.hollyhook.posehook

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

/**
 * Shows OK/Cancel confirmation dialog about camera permission.
 */
class PermissionConfirmationDialog(private val msg: Int,
                                   private val permId: Int,
                                   private val perm: String) : DialogFragment() {

  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
    AlertDialog.Builder(activity)
      .setMessage(msg)
      .setPositiveButton(android.R.string.ok) { _, _ ->
        activity!!.requestPermissions(
          arrayOf(perm),
          permId
        )
      }
      .setNegativeButton(android.R.string.cancel) { _, _ ->
        activity?.finish() // exit app
      }
      .create()
}
